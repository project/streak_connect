<?php

namespace Drupal\streak_connect\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Streak API client.
 */
class ApiClient {

  const API_URL = 'https://www.streak.com/api';

  /**
   * The logger interface.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The strike apikey.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * Constructs an ApiRequestService object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The HTTP client.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(LoggerChannelFactory $logger, ClientInterface $http_client, ConfigFactoryInterface $config_factory) {
    $this->logger = $logger->get('streak_connect');
    $this->httpClient = $http_client;
    $this->apiKey = $config_factory->get('streak_connect.settings')->get('api_key', '');
  }

  /**
   * Returns the headers required for making API requests.
   *
   * @return array
   *   An array of headers.
   */
  public function getHeaders() {
    return [
      'Authorization' => 'Basic ' . base64_encode($this->apiKey . ':'),
      'Content-Type' => 'application/json',
    ];
  }

  /**
   * Retrieves the teams for the current user.
   *
   * @return array|null
   *   The teams array if successful, FALSE otherwise.
   */
  public function getTeams() {
    try {
      $response = $this->httpClient->get(self::API_URL . '/v2/users/me/teams', [
        'headers' => $this->getHeaders(),
      ]);

      $teams = Json::decode($response->getBody());
      return $teams['results'];
    }
    catch (RequestException $e) {
      $this->logger->error('Streak: @message', ['@message' => $e->getMessage()]);
      return NULL;
    }
  }

  /**
   * Retrieves the pipelines for the current user.
   *
   * @return array|null
   *   The pipelines array if successful, FALSE otherwise.
   */
  public function getPipelines() {
    try {
      $response = $this->httpClient->get(self::API_URL . '/v1/pipelines', [
        'headers' => $this->getHeaders(),
      ]);

      $pipelines = Json::decode($response->getBody());
      return $pipelines;
    }
    catch (RequestException $e) {
      $this->logger->error('Streak: @message', ['@message' => $e->getMessage()]);
      return NULL;
    }
  }

  /**
   * Retrieves the fields of a pipeline.
   *
   * @return array|null
   *   The fields array if successful, FALSE otherwise.
   */
  public function getFields(string $pipelineKey) {
    try {
      $response = $this->httpClient->get(self::API_URL . '/v1/pipelines/' . $pipelineKey . '/fields', [
        'headers' => $this->getHeaders(),
      ]);

      $fields = Json::decode($response->getBody());
      return $fields;
    }
    catch (RequestException $e) {
      $this->logger->error('Streak: @message', ['@message' => $e->getMessage()]);
      return NULL;
    }
  }

  /**
   * Retrieves the boxes of a pipeline.
   *
   * @return array|null
   *   The boxes array if successful, FALSE otherwise.
   */
  public function getBoxes(string $pipelineKey) {
    try {
      $response = $this->httpClient->get(self::API_URL . '/v2/pipelines/' . $pipelineKey . '/boxes', [
        'headers' => $this->getHeaders(),
      ]);

      $boxes = Json::decode($response->getBody());
      return $boxes['results'];
    }
    catch (RequestException $e) {
      $this->logger->error('Streak: @message', ['@message' => $e->getMessage()]);
      return NULL;
    }
  }

  /**
   * Creates  new box in a pipeline.
   *
   * @return array|null
   *   The pipelines array if successful, FALSE otherwise.
   */
  public function createBox(string $pipelineKey, string $boxName) {
    try {
      $response = $this->httpClient->post(self::API_URL . '/v2/pipelines/' . $pipelineKey . '/boxes', [
        'headers' => $this->getHeaders(),
        'json' => [
          'name' => $boxName,
        ],
      ]);

      $box = Json::decode($response->getBody());
      return $box;
    }
    catch (RequestException $e) {
      $this->logger->error('Streak: @message', ['@message' => $e->getMessage()]);
      return NULL;
    }
  }


  /**
   * Update an specific box.
   *
   * @return array|null
   *   The pipelines array if successful, FALSE otherwise.
   */
  public function updateBox(string $boxKey, array $data) {
    try {
      $this->httpClient->post(self::API_URL . '/v1/boxes/' . $boxKey, [
        'headers' => $this->getHeaders(),
        'json' => $data,
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error('Streak: @message', ['@message' => $e->getMessage()]);
      return NULL;
    }
  }

  /**
   * Retrieves the pipelines for the current user.
   *
   * @return array|null
   *   The pipelines array if successful, FALSE otherwise.
   */
  public function updateBoxField(string $boxKey, string $fieldKey, $fieldValue) {
    try {
      $this->httpClient->post(self::API_URL . '/v1/boxes/' . $boxKey . '/fields/' . $fieldKey, [
        'headers' => $this->getHeaders(),
        'json' => [
          'value' => $fieldValue,
        ],
      ]);
    }
    catch (RequestException $e) {
      $this->logger->error('Streak: @message', ['@message' => $e->getMessage()]);
      return NULL;
    }
  }

  /**
   * Adds a new contact to the specified team.
   *
   * @param string $team_key
   *   The team key.
   * @param array $data
   *   The given name of the contact.
   *
   * @return array|bool
   *   The added contact array if successful, FALSE otherwise.
   */
  public function addContact(string $team_key, array $data) {

    if (!($data['email'] ?? FALSE)) {
      return FALSE;
    }

    $url = self::API_URL . "/v2/teams/{$team_key}/contacts/";

    $body = [
      'emailAddresses' => is_array($data['email']) ? $data['email'] : [$data['email']],
    ];

    $additional_fields = [
      'givenName',
      'familyName',
    ];

    foreach ($additional_fields as $field) {
      if ($data[$field] ?? FALSE) {
        $body[$field] = $data[$field];
      }
    }

    try {
      $response = $this->httpClient->post($url, [
        'headers' => $this->getHeaders(),
        'json' => $body,
      ]);

      $contact = Json::decode($response->getBody());
      return $contact;
    }
    catch (RequestException $e) {
      $this->logger->error('streak: @message', ['@message' => $e->getMessage()]);
      return FALSE;
    }
  }

}

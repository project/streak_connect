<?php

namespace Drupal\streak_connect\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Streak webform handler.
 *
 * @WebformHandler(
 *   id = "streak_webform_handler",
 *   label = @Translation("Streak"),
 *   category = @Translation("External"),
 *   description = @Translation("Sends a form submission to a Streak team."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class StreakWebformHandler extends WebformHandlerBase implements ContainerFactoryPluginInterface {

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * The HTTP client.
   *
   * @var \Drupal\streak\ApiClient
   */
  protected $apiClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->tokenManager = $container->get('webform.token_manager');
    $instance->apiClient = $container->get('streak.api_client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'team' => '',
      'email' => '',
      'pipeline' => '',
      'givenName' => '',
      'familyName' => '',
      'extraFields' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $teams = $this->apiClient->getTeams() ?? [];
    $team_options = [];

    foreach ($teams as $team) {
      $team_options[$team['key']] = $team['name'];
    }

    $pipelines = $this->apiClient->getPipelines() ?? [];
    $pipeline_options = [];

    foreach ($pipelines as $pipeline) {
      $pipeline_options[$pipeline['key']] = $pipeline['name'];
    }

    $values = $form_state->getValue('streak');
    $pipelineKey = $values['pipeline'] ?? $this->configuration['pipeline'];
    $streakFields = $pipelineKey ? $this->apiClient->getFields($pipelineKey) : [];

    if ($pipelineKey) {
      $boxes_options['new'] = $this->t('Create new box.');
    }
    else {
      $boxes_options['empty'] = $this->t('Select a pipeline first.');
    }

    foreach ($boxes as $box) {
      $boxes_options[$box['key']] = $box['name'];
    }

    $form['streak'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Streak settings'),
    ];

    $form['streak']['team'] = [
      '#type' => 'select',
      '#title' => $this->t('Team'),
      '#options' => $team_options,
      '#description' => $this->t('Select the team.'),
      '#default_value' => $this->configuration['team'] ?? '',
      '#required' => TRUE,
    ];

    $form['streak']['pipeline'] = [
      '#type' => 'select',
      '#title' => $this->t('Pipeline'),
      '#options' => $pipeline_options,
      '#empty_option' => '- Select an option -',
      '#description' => $this->t('Select the pipeline.'),
      '#default_value' => $this->configuration['pipeline'] ?? '',
      '#ajax' => [
        'callback' => [$this, 'updateExtraFields'],
        'wrapper' => 'webform-streak-fields-handler-settings',
      ],
    ];

    $fields = $this->getWebform()->getElementsInitializedAndFlattened();
    $email_options = [];
    $full_options = [];
    foreach ($fields as $field_name => $field) {
      $full_options[$field_name] = $field['#title'];
      if (in_array($field['#type'], ['email', 'webform_email_confirm'])) {
        $email_options[$field_name] = $field['#title'];
      }
    }

    $form['streak']['email'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['email'] ?? '',
      '#options' => $email_options,
      '#empty_option' => $this->t('- Select an option -'),
      '#description' => $this->t('Select the email element you want to send to Streak.'),
    ];

    $form['streak']['givenName'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('Given Name'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['givenName'] ?? '',
      '#options' => $full_options,
      '#empty_option' => $this->t('- Select an option -'),
    ];

    $form['streak']['familyName'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('Family Name'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['familyName'] ?? '',
      '#options' => $full_options,
      '#empty_option' => $this->t('- Select an option -'),
    ];

    $form['streak']['extraFields'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Additional fields'),
      '#attributes' => ['id' => 'webform-streak-fields-handler-settings'],
    ];


    foreach ($streakFields as $streakField) {
      $form['streak']['extraFields'][$streakField['key']] = [
        '#type' => 'webform_select_other',
        '#title' => $streakField['name'],
        '#required' => FALSE,
        '#default_value' => $this->configuration['extraFields'][$streakField['key']] ?? '',
        '#options' => $full_options,
        '#empty_option' => $this->t('- Select an option -'),
      ];
    }


    $form['streak']['token_tree_link'] = $this->tokenManager->buildTreeLink();


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration = $form_state->getValue('streak');
  }

  /**
   * {@inheritdoc}
   */
  public function updateExtraFields(array &$form, FormStateInterface $form_state) {
    return $form['settings']['streak']['extraFields'];
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {

    if ($update) {
      return;
    }

    $fields = $webform_submission->toArray(TRUE);
    $configuration = $this->tokenManager->replace($this->configuration, $webform_submission);
    $data = [];

    $extraFields = $configuration['extraFields'] ?? [];

    foreach ($configuration as $configKey => $configValue) {
      if ($configKey == 'extraFields') {
        continue;
      }

      if ($fields['data'][$configValue] ?? FALSE) {
        $data[$configKey] = $fields['data'][$configValue];
      }
    }

    foreach ($extraFields as $fieldID => $fieldValue) {
      if ($fields['data'][$fieldValue] ?? FALSE) {
        $value = $fields['data'][$fieldValue];
        $value = is_array($value) ? implode(', ', $value) : $value;
        $extraFields[$fieldID] = $value;
      }
      else {
        unset($extraFields[$fieldID]);
      }
    }

    $team = $data['team'] ?? $configuration['team'];

    $contact = $this->apiClient->addContact($team, $data);

    if ($this->configuration['pipeline']) {
      $fullName = $contact['emailAddress'][0];
      if ($contact['givenName'] && $contact['familyName']) {
        $fullName = $data['givenName'] . ' ' . $data['familyName'];
      }

      $box = $this->apiClient->createBox($this->configuration['pipeline'], $fullName);

      if ($box) {
        $this->apiClient->updateBox($box['key'], [
          'contacts' => [
            ['key' => $contact['key'], 'isStarred' => TRUE],
          ],
        ]);

        foreach ($extraFields as $field => $value) {
          $this->apiClient->updateBoxField($box['key'], $field, $value);
        }
      }
    }
  }

}
